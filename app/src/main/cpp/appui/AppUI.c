#include <AEEModGen.h>
#include <AEEAppGen.h>
#include <AEEHeap.h>
#include <AEEFile.h>
#include <AEEStdLib.h>
#include <AEEAppletCtl.h>

#include "Menu.h"

typedef struct {
    AEEApplet applet;
    CMenuManager menuManager;
    CMenu mainMenu;
    CMenu appActionMenu;
    CMenu systemInformationMenu;
    CMenu fileBrowserMenu;
    CMenu errorMessageMenu;
    CMenuEntryGroup *applicationsGroup;
    CMenuEntryGroup *ramStatusGroup;
    CMenuEntryGroup *fileBrowserListGroup;
    CMenuEntryGroup *errorMessageGroup;
    boolean showHiddenApps;
    char fileBrowserCurrentDirectory[AEE_MAX_FILE_NAME];
} CAppUIApp;

typedef struct {
    CAppUIApp *app;
    AEECLSID classID;
} AppStartupInfo;

static void ShowErrorMessage(CAppUIApp *app, const AECHAR *message);

static void StartupApp(CMenuEntry *entry, void *info_) {
    AppStartupInfo *info = (AppStartupInfo *) info_;
    int status = ISHELL_StartApplet(info->app->applet.m_pIShell, info->classID);
    if (SUCCESS != status) {
        AECHAR errorMessage[64];
        WSPRINTF(errorMessage, 64, (const AECHAR *) u"Cannot start applet (%d)", status);
        ShowErrorMessage(info->app, errorMessage);
    }
}

static void AppUI_FreeAppData(CAppUIApp *app);

static void AppUI_ReloadAppList(CAppUIApp *app);

static const AECHAR *const SHOW_HIDDEN_APPS_OFF = (const AECHAR *) u"Show Hidden Apps: off";
static const AECHAR *const SHOW_HIDDEN_APPS_ON = (const AECHAR *) u"Show Hidden Apps: on";

static void ShowHiddenAppsEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;
    app->showHiddenApps = !app->showHiddenApps;
    CMenuEntry_UpdateLabel(entry, app->showHiddenApps ? SHOW_HIDDEN_APPS_ON : SHOW_HIDDEN_APPS_OFF);
    AppUI_ReloadAppList(app);
    CMenuManager_FixScroll(&app->menuManager);
    CMenuManager_Render(&app->menuManager);
}

static void ShowSystemInformationEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;

    CMenuEntryGroup_Clear(app->ramStatusGroup);

    AECHAR strRamUsed[32];
    AECHAR strRamTotal[32];

    IHeap *heap = NULL;
    if (SUCCESS == ISHELL_CreateInstance(app->applet.m_pIShell, AEECLSID_HEAP, (void **) &heap)) {
        WSPRINTF(strRamUsed, sizeof(strRamUsed), (const AECHAR *) u"RAM Used: %dk",
                 IHEAP_GetMemStats(heap) / 1024);
        IHEAP_Release(heap);
    } else {
        WSTRCPY(strRamUsed, (const AECHAR *) u"RAM Used: unknown");
    }

    AEEDeviceInfo deviceInfo;
    deviceInfo.wStructSize = sizeof(deviceInfo);
    ISHELL_GetDeviceInfo(app->applet.m_pIShell, &deviceInfo);

    WSPRINTF(strRamTotal, sizeof(strRamTotal), (const AECHAR *) u"RAM Total: %dk",
             deviceInfo.dwRAM / 1024);

    CMenuEntry *newEntry = NULL;
    CMenuEntryGroup_AddNewEntry(app->ramStatusGroup, strRamUsed, &newEntry);
    CMenuEntryGroup_AddNewEntry(app->ramStatusGroup, strRamTotal, &newEntry);
    CMenuManager_OpenMenu(&app->menuManager, &app->systemInformationMenu);
}

struct FileEntryData {
    CAppUIApp *app;
    FileInfo info;
};

static void ShowFileBrowserDirectory(CAppUIApp *app, const char *directory);

static void SelectFileBrowserListEntry(CMenuEntry *entry, void *data_) {
    struct FileEntryData *data = (struct FileEntryData *) data_;
    if (data->info.attrib & AEE_FA_DIR) {
        ShowFileBrowserDirectory(data->app, data->info.szName);
    }
}

static void ReleaseFileBrowserListEntry(CMenuEntry *entry, void *data) {
    FREE(data);
}

static void AddFileBrowserListEntryEx(CAppUIApp *app, CMenuEntryGroup *group, const AECHAR *name, FileInfo *info) {
    CMenuEntry *fileEntry = NULL;
    if (SUCCESS == CMenuEntryGroup_AddNewEntry(group, name, &fileEntry)) {
        struct FileEntryData *data = MALLOC(sizeof(struct FileEntryData));
        data->app = app;
        data->info = *info;
        fileEntry->userData = data;
        fileEntry->releaseCallback = ReleaseFileBrowserListEntry;
        fileEntry->callback = SelectFileBrowserListEntry;
    }
}

static void AddFileBrowserListEntry(CAppUIApp *app, CMenuEntryGroup *group, FileInfo *info) {
    const char *aName = STRRCHR(info->szName, '/');
    if (!aName) {
        aName = info->szName;
    } else {
        aName++;
    }
    AECHAR name[sizeof(info->szName)];
    STREXPAND((const byte *) aName, STRLEN(aName) + 1, name, sizeof(name));
    AddFileBrowserListEntryEx(app, group, name, info);
}

static boolean GetParentDirectoryInfo(const char *directory, FileInfo *info) {
    info->attrib = AEE_FA_DIR;
    const char *pLastSlash = STRRCHR(directory, '/');
    if (!pLastSlash || STRCMP(directory, "fs:/") == 0) {
        STRCPY(info->szName, "fs:/");
        return FALSE;
    } else {
        STRNCPY(info->szName, directory, pLastSlash - directory);
        info->szName[pLastSlash - directory] = 0;
        if (STRCMP(info->szName, "fs:") == 0) {
            STRCPY(info->szName, "fs:/");
            return TRUE;
        }
    }
    info->dwSize = 0;
    info->dwCreationDate = 0;
    return TRUE;
}

static void ShowFileBrowserDirectory(CAppUIApp *app, const char *directory) {
    CMenuEntryGroup_Clear(app->fileBrowserListGroup);

    STRCPY(app->fileBrowserCurrentDirectory, directory);

    FileInfo parentInfo;
    if (GetParentDirectoryInfo(directory, &parentInfo)) {
        AddFileBrowserListEntryEx(app, app->fileBrowserListGroup, (const AECHAR *) u"..",
                                  &parentInfo);
    }

    IFileMgr *fm = NULL;
    if (SUCCESS ==
        ISHELL_CreateInstance(app->applet.m_pIShell, AEECLSID_FILEMGR, (void **) &fm)) {
        if (SUCCESS == IFILEMGR_EnumInit(fm, directory, TRUE)) {
            FileInfo info;
            while (IFILEMGR_EnumNext(fm, &info)) {
                AddFileBrowserListEntry(app, app->fileBrowserListGroup, &info);
            }
        }
        if (SUCCESS == IFILEMGR_EnumInit(fm, directory, FALSE)) {
            FileInfo info;
            while (IFILEMGR_EnumNext(fm, &info)) {
                AddFileBrowserListEntry(app, app->fileBrowserListGroup, &info);
            }
        }
    }

    CMenuManager_OpenMenu(&app->menuManager, &app->fileBrowserMenu);
}

static void ErrorMessageEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;

    CMenu *menu = entry->parent->parent;
    if (menu->backHandler) menu->backHandler(&app->menuManager, menu, menu->userData);
}

static void ShowErrorMessage(CAppUIApp *app, const AECHAR *message) {
    CMenuEntryGroup_Clear(app->errorMessageGroup);

    CMenuEntry *errorEntry = NULL;
    if (SUCCESS == CMenuEntryGroup_AddNewEntry(app->errorMessageGroup, message, &errorEntry)) {
        errorEntry->userData = app;
        errorEntry->callback = ErrorMessageEntryCallback;
    }

    CMenuManager_OpenMenu(&app->menuManager, &app->errorMessageMenu);
}

static void ShowFileBrowserEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;
    ShowFileBrowserDirectory(app, "fs:/");
}

static void FileBrowserBackHandler(CMenuManager *manager, CMenu *menu, void *userData) {
    CAppUIApp *app = (CAppUIApp *) userData;
    FileInfo info;
    if (GetParentDirectoryInfo(app->fileBrowserCurrentDirectory, &info)) {
        ShowFileBrowserDirectory(app, info.szName);
    } else {
        CMenuManager_OpenMenu(&app->menuManager, &app->mainMenu);
    }
}

static void LaunchAppEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;

    CMenuManager_OpenMenu(&app->menuManager, &app->mainMenu);

    StartupApp(entry, entry->parent->parent->userData);
}

struct TarHeader {
    char fileName[100];
    char fileMode[8];
    char uid[8];
    char gid[8];
    char fileSize[12];
    char mtime[12];
    char checksum[8];
    char type;
    char linkedFileName[100];
    char ustar[6];
    char ustarVer[2];
    char uname[32];
    char gname[32];
    char major[8];
    char minor[8];
    char fileNamePrefix[155];
    char garbage[12];
};

static void EncodeTarNumber(char *dst, size_t length, uint32 value) {
    for (size_t i = 0; i < length - 1; i++) {
        dst[i] = '0' + ((value >> ((length - 2 - i) * 3u)) & 7u);
    }
    dst[length - 1] = '\0';
}

static int StoreFileInTar(IFileMgr *fm, IFile *tarFile, const char *filename) {
    IFile *file = IFILEMGR_OpenFile(fm, filename, _OFM_READ);
    if (!file) return EFILENOEXISTS;

    AEEFileInfo info;
    IFILE_GetInfo(file, &info);

    // allocate on heap to avoid polluting the stack
    struct TarHeader *tarHeader = MALLOC(sizeof(struct TarHeader));

    STRNCPY(tarHeader->fileName, filename, 100);
    EncodeTarNumber(tarHeader->fileMode, 8, 0100777);
    EncodeTarNumber(tarHeader->uid, 8, 0);
    EncodeTarNumber(tarHeader->gid, 8, 0);
    EncodeTarNumber(tarHeader->fileSize, 12, info.dwSize);
    EncodeTarNumber(tarHeader->mtime, 12, 0);
    MEMSET(tarHeader->checksum, ' ', 8);
    tarHeader->type = '0';
    MEMSET(tarHeader->linkedFileName, 0, 100);
    MEMCPY(tarHeader->ustar, "ustar\0", 6);
    MEMCPY(tarHeader->ustarVer, "00", 2);
    MEMSET(tarHeader->uname, 0, 32);
    MEMSET(tarHeader->gname, 0, 32);
    MEMSET(tarHeader->major, 0, 8);
    MEMSET(tarHeader->minor, 0, 8);
    MEMSET(tarHeader->fileNamePrefix, 0, 155);

    unsigned char *tarHeaderBytes = (unsigned char *) tarHeader;
    uint32 checksum = 0;
    for (size_t i = 0; i < sizeof(struct TarHeader); i++) {
        checksum += tarHeaderBytes[i];
    }
    EncodeTarNumber(tarHeader->checksum, 8, checksum);

    IFILE_Write(tarFile, tarHeaderBytes, sizeof(struct TarHeader));
    FREE(tarHeader);

    uint32 totalRead = 0;
    char *buf = MALLOC(4096);
    while (TRUE) {
        int32 numRead = IFILE_Read(file, buf, 4096);
        if (numRead > 0) {
            IFILE_Write(tarFile, buf, numRead); // TODO: check any errors
            totalRead += numRead;
        } else {
            break;
        }
    }
    uint32 remaining = totalRead & 0x1FFu;
    if (remaining != 0) {
        remaining = 0x200 - remaining;
    }
    MEMSET(buf, 0, remaining);
    IFILE_Write(tarFile, buf, remaining);
    FREE(buf);
    IFILE_Release(file);
    return SUCCESS;
}

struct DirectoryRecord {
    char filename[AEE_MAX_FILE_NAME];
    struct DirectoryRecord *next;
};

static int StoreDirectoryInTar(IFileMgr *fm, IFile *tarFile, const char *dirname) {
    if (SUCCESS == IFILEMGR_EnumInit(fm, dirname, FALSE)) {
        FileInfo fileInfo;
        while (IFILEMGR_EnumNext(fm, &fileInfo)) {
            StoreFileInTar(fm, tarFile, fileInfo.szName);
        }
    }

    struct DirectoryRecord *recordFirst = NULL;
    struct DirectoryRecord *recordLast = NULL;

    if (SUCCESS == IFILEMGR_EnumInit(fm, dirname, TRUE)) {
        FileInfo fileInfo;
        while (IFILEMGR_EnumNext(fm, &fileInfo)) {
            struct DirectoryRecord *recordPrev = recordLast;

            recordLast = MALLOC(sizeof(struct DirectoryRecord));
            STRNCPY(recordLast->filename, fileInfo.szName, sizeof(recordLast->filename));
            recordLast->filename[sizeof(recordLast->filename) - 1] = 0;
            recordLast->next = NULL;

            if (recordPrev) {
                recordPrev->next = recordLast;
            } else {
                recordFirst = recordLast;
            }
        }
    }

    struct DirectoryRecord *recordCur = recordFirst;
    while (recordCur) {
        StoreDirectoryInTar(fm, tarFile, recordCur->filename);

        recordCur = recordCur->next;
    }

    recordCur = recordFirst;
    while (recordCur) {
        struct DirectoryRecord *recordNext = recordCur->next;
        FREE(recordCur);

        recordCur = recordNext;
    }

    return SUCCESS;
}

static void BackupAppEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;

    AppStartupInfo *info = (AppStartupInfo *) entry->parent->parent->userData;

    AEEAppInfo appInfo;
    if (ISHELL_QueryClass(app->applet.m_pIShell, info->classID, &appInfo)) {
        IFileMgr *fm;
        if (SUCCESS ==
            ISHELL_CreateInstance(app->applet.m_pIShell, AEECLSID_FILEMGR, (void **) &fm)) {
            const char *tmp = STRRCHR(appInfo.pszMIF, '/');
            if (!tmp) {
                tmp = appInfo.pszMIF;
            } else {
                tmp++;
            }

            char moduleName[AEE_MAX_FILE_NAME];
            STRNCPY(moduleName, tmp, AEE_MAX_FILE_NAME);
            moduleName[AEE_MAX_FILE_NAME - 1] = 0;
            char *tmp2 = STRRCHR(moduleName, '.');
            if (tmp2) *tmp2 = 0;

            char backupFileName[AEE_MAX_FILE_NAME];
            SNPRINTF(backupFileName, AEE_MAX_FILE_NAME, "fs:/shared/%s.tar", moduleName);

            char modDirName[AEE_MAX_FILE_NAME];
            SNPRINTF(modDirName, AEE_MAX_FILE_NAME, "fs:/mod/%s/", moduleName);

            IFILEMGR_Remove(fm, backupFileName);
            IFile *wfile = IFILEMGR_OpenFile(fm, backupFileName, _OFM_CREATE);
            if (wfile) {
                StoreFileInTar(fm, wfile, appInfo.pszMIF);
                StoreDirectoryInTar(fm, wfile, modDirName);

                char *buf = MALLOC(1024);
                IFILE_Write(wfile, buf, 1024);
                FREE(buf);

                IFILE_Release(wfile);
            }
            IFILEMGR_Release(fm);
        }
    }

    CMenuManager_OpenMenu(&app->menuManager, &app->mainMenu);
}

#ifdef AEE_STATIC
static void ReloadModulesEntryCallback(CMenuEntry *entry, void *app_) {
    CAppUIApp *app = (CAppUIApp *) app_;

    extern int AEE_LoadModuleList();

    AEE_LoadModuleList();
    IAppletCtl *appletCtl = NULL;
    if (SUCCESS == ISHELL_CreateInstance(app->applet.m_pIShell, AEECLSID_APPLETCTL, (void **) &appletCtl)) {
        int appletListSize = 0;
        int status = IAPPLETCTL_GetRunningList(appletCtl, NULL, &appletListSize);
        if (status == SUCCESS || status == EBUFFERTOOSMALL) {
            uint32 *appletIDs = (uint32 *) MALLOC(appletListSize);
            status = IAPPLETCTL_GetRunningList(appletCtl, appletIDs, &appletListSize);
            if (status == SUCCESS) {
                for (uint32 i = 0; i < appletListSize / sizeof(uint32); i++) {
                    DBGPRINTF("sending reload event to applet %08x", appletIDs[i]);
                    ISHELL_SendEvent(app->applet.m_pIShell, appletIDs[i], EVT_MOD_LIST_CHANGED, 0, 0);
                }
            }
        }
        IAPPLETCTL_Release(appletCtl);
    }
    ISHELL_Notify(app->applet.m_pIShell, AEECLSID_SHELL, NMASK_SHELL_MOD_LIST_CHANGED, 0);
}
#endif

static void AppUI_MainBackHandler(CMenuManager *manager, CMenu *menu, void *userData) {
    CAppUIApp *app = (CAppUIApp *) userData;
    ISHELL_CloseApplet(app->applet.m_pIShell, FALSE);
}

static boolean AppUI_InitAppData(CAppUIApp *app) {
    /* try */ {
        if (SUCCESS != CMenuManager_Init(&app->menuManager, app->applet.m_pIShell)) goto onError;

        {
            CMenu_Init(&app->mainMenu);
            app->mainMenu.backHandler = AppUI_MainBackHandler;
            app->mainMenu.userData = app;

            if (SUCCESS != CMenu_AddNewGroup(&app->mainMenu, &app->applicationsGroup)) goto onError;
            app->applicationsGroup->groupLabel = (const AECHAR *) u"Applications";

            CMenuEntryGroup *optionsGroup;
            if (SUCCESS != CMenu_AddNewGroup(&app->mainMenu, &optionsGroup)) goto onError;
            optionsGroup->groupLabel = (const AECHAR *) u"Options";

#if 0
            CMenuEntry *testEntry = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(optionsGroup, NULL, &testEntry)) goto onError;
            CMenuEntry_SetTextInput(testEntry, app->applet.m_pIShell, NULL);
#endif

            CMenuEntry *showHiddenAppsEntry = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(optionsGroup,
                                                       (const AECHAR *) u"Show Hidden Apps: off",
                                                       &showHiddenAppsEntry))
                goto onError;

            showHiddenAppsEntry->callback = ShowHiddenAppsEntryCallback;
            showHiddenAppsEntry->userData = app;

#ifdef AEE_STATIC
            CMenuEntry *reloadModules = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(optionsGroup,
                                                       (const AECHAR *) u"Reload Module List",
                                                       &reloadModules))
                goto onError;

            reloadModules->callback = ReloadModulesEntryCallback;
            reloadModules->userData = app;
#endif

            CMenuEntry *fileBrowserEntry = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(optionsGroup,
                                                       (const AECHAR *) u"File Manager...",
                                                       &fileBrowserEntry))
                goto onError;

            fileBrowserEntry->callback = ShowFileBrowserEntryCallback;
            fileBrowserEntry->userData = app;

            CMenuEntry *systemInfoEntry = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(optionsGroup,
                                                       (const AECHAR *) u"System Information...",
                                                       &systemInfoEntry))
                goto onError;

            systemInfoEntry->callback = ShowSystemInformationEntryCallback;
            systemInfoEntry->userData = app;
        }

        {
            CMenu_Init(&app->appActionMenu);

            CMenuEntryGroup *actionsGroup;
            if (SUCCESS != CMenu_AddNewGroup(&app->appActionMenu, &actionsGroup)) goto onError;
            actionsGroup->groupLabel = (const AECHAR *) u"Actions";

            CMenuEntry *launchAppEntry = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(actionsGroup,
                                                       (const AECHAR *) u"Launch App",
                                                       &launchAppEntry))
                goto onError;

            launchAppEntry->callback = LaunchAppEntryCallback;
            launchAppEntry->userData = app;

            CMenuEntry *backupAppEntry = NULL;
            if (SUCCESS != CMenuEntryGroup_AddNewEntry(actionsGroup,
                                                       (const AECHAR *) u"Backup App",
                                                       &backupAppEntry))
                goto onError;

            backupAppEntry->callback = BackupAppEntryCallback;
            backupAppEntry->userData = app;

            app->appActionMenu.backMenu = &app->mainMenu;
        }

        {
            CMenu_Init(&app->systemInformationMenu);

            CMenuEntryGroup *brewVersionGroup;
            if (SUCCESS != CMenu_AddNewGroup(&app->systemInformationMenu, &brewVersionGroup))
                goto onError;
            brewVersionGroup->groupLabel = (const AECHAR *) u"BREW Version";

            AECHAR versionInfo[32];
            GETAEEVERSION((byte *) versionInfo, sizeof(versionInfo), GAV_MSM | GAV_UPDATE);

            CMenuEntry *brewVersionEntry = NULL;
            CMenuEntryGroup_AddNewEntry(brewVersionGroup, versionInfo, &brewVersionEntry);

            if (SUCCESS != CMenu_AddNewGroup(&app->systemInformationMenu, &app->ramStatusGroup))
                goto onError;
            app->ramStatusGroup->groupLabel = (const AECHAR *) u"RAM Usage";

            app->systemInformationMenu.backMenu = &app->mainMenu;
        }

        {
            CMenu_Init(&app->fileBrowserMenu);

            if (SUCCESS != CMenu_AddNewGroup(&app->fileBrowserMenu, &app->fileBrowserListGroup))
                goto onError;
            app->fileBrowserListGroup->groupLabel = (const AECHAR *) u"Files";

            app->fileBrowserMenu.backHandler = FileBrowserBackHandler;
            app->fileBrowserMenu.userData = app;
        }

        {
            CMenu_Init(&app->errorMessageMenu);

            if (SUCCESS != CMenu_AddNewGroup(&app->errorMessageMenu, &app->errorMessageGroup))
                goto onError;
            app->errorMessageGroup->groupLabel = (const AECHAR *) u"Error";

            app->fileBrowserMenu.backHandler = FileBrowserBackHandler;
            app->fileBrowserMenu.userData = app;
        }

        return TRUE;
    }
    onError:
    AppUI_FreeAppData(app);
    return FALSE;
}

static void AppUI_FreeAppData(CAppUIApp *app) {
    app->applicationsGroup = NULL;
    CMenuManager_Free(&app->menuManager);
    CMenu_Free(&app->mainMenu);
    CMenu_Free(&app->appActionMenu);
    CMenu_Free(&app->systemInformationMenu);
    CMenu_Free(&app->fileBrowserMenu);
    CMenu_Free(&app->errorMessageMenu);
}

static CMenu *ShowAppActions(CMenuEntry *entry, void *info_) {
    AppStartupInfo *info = (AppStartupInfo *) info_;

    info->app->appActionMenu.userData = info;

    return &info->app->appActionMenu;
}

static void ReleaseStartupInfo(CMenuEntry *entry, void *info) {
    FREE(info);
}

static void AppUI_ReloadAppList(CAppUIApp *app) {
    CMenuEntryGroup_Clear(app->applicationsGroup);

    IShell *shell = app->applet.m_pIShell;

    ISHELL_EnumAppletInit(shell);
    AEEAppInfo appInfo;
    while (ISHELL_EnumNextApplet(shell, &appInfo)) {
        if (!app->showHiddenApps &&
            ((appInfo.wFlags & AFLAG_HIDDEN) || (appInfo.wFlags & AFLAG_SCREENSAVER)))
            continue;
        if (appInfo.cls == app->applet.clsID) continue;

        AECHAR appName[64];
        if (!ISHELL_LoadResString(shell, appInfo.pszMIF, APPR_NAME(appInfo), appName,
                                  sizeof(appName))) {
            DBGPRINTF("Could not load app name for applet CLSID:0x%08x", appInfo.cls);
            continue;
        }

        IImage *icon = ISHELL_LoadResImage(shell, appInfo.pszMIF, APPR_THUMB(appInfo));

        AppStartupInfo *startupInfo = MALLOC(sizeof(AppStartupInfo));
        startupInfo->app = app;
        startupInfo->classID = appInfo.cls;

        CMenuEntry *entry = NULL;
        if (SUCCESS == CMenuEntryGroup_AddNewEntry(app->applicationsGroup, appName, &entry)) {
            CMenuEntry_SetIcon(entry, icon);
            entry->userData = startupInfo;
            entry->releaseCallback = ReleaseStartupInfo;
            entry->submenuCallback = ShowAppActions;
            entry->callback = StartupApp;
        }

        if (icon) IImage_Release(icon);
    }
    CMenuEntryGroup_Sort(app->applicationsGroup);
}

static boolean AppUI_HandleEvent(CAppUIApp *app, AEEEvent eCode, uint16 wParam, uint32 dwParam) {
    if (eCode == EVT_APP_START) {
        app->showHiddenApps = FALSE;
        AppUI_ReloadAppList(app);
        CMenuManager_OpenMenu(&app->menuManager, &app->mainMenu);
        return TRUE;
    } else if (eCode == EVT_APP_RESUME) {
        CMenuManager_Render(&app->menuManager);
        return TRUE;
    } else if (eCode == EVT_APP_SUSPEND) {
        return TRUE;
    } else if (eCode == EVT_APP_STOP) {
        return TRUE;
    } else if (CMenuManager_HandleEvent(&app->menuManager, eCode, wParam, dwParam)) {
        return TRUE;
    } else if (eCode == EVT_MOD_LIST_CHANGED) {
        AppUI_ReloadAppList(app);
        CMenuManager_Render(&app->menuManager);
        return TRUE;
    } else if (eCode == EVT_NOTIFY) {
        AEENotify *notify = (AEENotify *)dwParam;
        if (notify->cls == AEECLSID_SHELL && (notify->dwMask & NMASK_SHELL_INIT)) {
            ISHELL_StartApplet(app->applet.m_pIShell, app->applet.clsID);
            notify->st = NSTAT_PROCESSED;
        }
    }

    return FALSE;
}

int AppUI_CreateInstance(AEECLSID ClsId, IShell *pIShell, IModule *pMod, void **ppobj) {
    if (!AEEApplet_New(sizeof(CAppUIApp), ClsId, pIShell, pMod, (IApplet **) ppobj,
                       (AEEHANDLER) AppUI_HandleEvent,
                       (PFNFREEAPPDATA) AppUI_FreeAppData)) {
        return ENOMEMORY;
    }

    CAppUIApp *app = (CAppUIApp *) *ppobj;

    if (!AppUI_InitAppData(app)) {
        *ppobj = NULL;
        return EFAILED;
    }
    return SUCCESS;
}

#ifdef AEE_STATIC

int AppUI_Load(IShell *shell, void *ph, IModule *ppMod) {
    return AEEStaticMod_New(sizeof(AEEMod), shell, ph, ppMod,
                            (PFNMODCREATEINST) AppUI_CreateInstance,
                            (PFNFREEMODDATA) NULL);
}

#else

int AEEClsCreateInstance(AEECLSID ClsId, IShell *pIShell, IModule *po, void **ppObj) {
    return AppUI_CreateInstance(ClsId, pIShell, po, ppObj);
}

#endif