#include "breInput.h"
#include "breScreenshot.h"

#include <AEEVCodes.h>
#include <android/input.h>
#include <AEE_OEMEvent.h>

static AVKType translateKeycode(jint keyCode) {
    int avk = AVK_UNDEFINED;
    switch(keyCode) {
        case AKEYCODE_DPAD_DOWN: avk = AVK_DOWN; break;
        case AKEYCODE_DPAD_UP: avk = AVK_UP; break;
        case AKEYCODE_DPAD_LEFT: avk = AVK_LEFT; break;
        case AKEYCODE_DPAD_RIGHT: avk = AVK_RIGHT; break;

        case AKEYCODE_DPAD_CENTER:
        case AKEYCODE_ENTER:
            avk = AVK_SELECT; break;

        case AKEYCODE_CLEAR:
        case AKEYCODE_ESCAPE:
        case AKEYCODE_C:
        case AKEYCODE_DEL:
            avk = AVK_CLR; break;

        case AKEYCODE_MENU:
        case AKEYCODE_W:
            avk = AVK_SOFT1; break;

        case AKEYCODE_BACK:
        case AKEYCODE_E:
            avk = AVK_SOFT2; break;

        case AKEYCODE_X:
        case AKEYCODE_ENDCALL:
            avk = AVK_END; break;

        case AKEYCODE_Z:
        case AKEYCODE_CALL:
            avk = AVK_SEND; break;

        case AKEYCODE_0:
        case AKEYCODE_NUMPAD_0:
            avk = AVK_0; break;

        case AKEYCODE_1:
        case AKEYCODE_NUMPAD_1:
            avk = AVK_1; break;

        case AKEYCODE_2:
        case AKEYCODE_NUMPAD_2:
            avk = AVK_2; break;

        case AKEYCODE_3:
        case AKEYCODE_NUMPAD_3:
            avk = AVK_3; break;

        case AKEYCODE_4:
        case AKEYCODE_NUMPAD_4:
            avk = AVK_4; break;

        case AKEYCODE_5:
        case AKEYCODE_NUMPAD_5:
            avk = AVK_5; break;

        case AKEYCODE_6:
        case AKEYCODE_NUMPAD_6:
            avk = AVK_6; break;

        case AKEYCODE_7:
        case AKEYCODE_NUMPAD_7:
            avk = AVK_7; break;

        case AKEYCODE_8:
        case AKEYCODE_NUMPAD_8:
            avk = AVK_8; break;

        case AKEYCODE_9:
        case AKEYCODE_NUMPAD_9:
            avk = AVK_9; break;

        case AKEYCODE_STAR:
        case AKEYCODE_S:
        case AKEYCODE_NUMPAD_MULTIPLY:
            avk = AVK_STAR; break;

        case AKEYCODE_POUND:
        case AKEYCODE_D:
        case AKEYCODE_NUMPAD_DIVIDE:
            avk = AVK_POUND; break;
    }

    return avk;
}

JNIEXPORT jboolean JNICALL
Java_io_github_usernameak_brewemulator_MainActivity_dispatchKeyEvent(JNIEnv *env, jobject thiz,
                                                                     jobject event) {
    jclass keyEventClass = (*env)->FindClass(env, "android/view/KeyEvent");
    jmethodID getKeyCode = (*env)->GetMethodID(env, keyEventClass, "getKeyCode", "()I");
    jmethodID getAction = (*env)->GetMethodID(env, keyEventClass, "getAction", "()I");

    jint keyCode = (*env)->CallIntMethod(env, event, getKeyCode);
    jint action = (*env)->CallIntMethod(env, event, getAction);

    AVKType avk = translateKeycode(keyCode);
    if (avk != AVK_UNDEFINED) {
        if (action == AKEY_EVENT_ACTION_DOWN) {
            AEE_Key(avk);
            AEE_KeyPress(avk);
            return JNI_TRUE;
        } else if (action == AKEY_EVENT_ACTION_UP) {
            AEE_KeyRelease(avk);
            return JNI_TRUE;
        }
    }
    return JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_io_github_usernameak_brewemulator_MainActivity_brewEmuOnScreenKeyDown(JNIEnv *env,
                                                                           jobject thiz,
                                                                           jint avk) {
    if(avk == AVK_UNDEFINED) {
        return JNI_FALSE;
    }
    if(avk == AVK_LAST + 1) {
        breScreenshot();
        return JNI_TRUE;
    }
    AEE_Key(avk);
    AEE_KeyPress(avk);
    return JNI_TRUE;
}

JNIEXPORT jboolean JNICALL
Java_io_github_usernameak_brewemulator_MainActivity_brewEmuOnScreenKeyUp(JNIEnv *env, jobject thiz,
                                                                         jint avk) {
    if(avk == AVK_UNDEFINED) {
        return JNI_FALSE;
    }
    if(avk == AVK_LAST + 1) {
        return JNI_TRUE;
    }
    AEE_KeyRelease(avk);
    return JNI_TRUE;
}

JNIEXPORT void JNICALL
Java_io_github_usernameak_brewemulator_InputDeviceListenerImpl_onInputDeviceAdded(JNIEnv *env,
                                                                                  jobject thiz,
                                                                                  jint deviceId) {

}

JNIEXPORT void JNICALL
Java_io_github_usernameak_brewemulator_InputDeviceListenerImpl_onInputDeviceRemoved(JNIEnv *env,
                                                                                    jobject thiz,
                                                                                    jint deviceId) {
    
}
