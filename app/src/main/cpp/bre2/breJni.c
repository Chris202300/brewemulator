#include "breJni.h"

#include <threads.h>

JavaVM *gJVM = NULL;

static tss_t jniEnvTssKey = NULL;

static void jniEnvDtor(void *p) {
    (*gJVM)->DetachCurrentThread(gJVM);
}

JNIEnv *breGetThreadLocalJNIEnv() {
    if (!jniEnvTssKey) {
        if (thrd_success != tss_create(&jniEnvTssKey, jniEnvDtor)) return NULL;
    }
    JNIEnv *env = (JNIEnv *) tss_get(jniEnvTssKey);
    if (!env) {
        if (JNI_OK == (*gJVM)->AttachCurrentThread(gJVM, &env, NULL)) {
            tss_set(jniEnvTssKey, env);
        } else {
            env = NULL;
        }
    }
    return env;
}
