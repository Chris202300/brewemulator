#ifndef BREWEMULATOR_BREJNI_H
#define BREWEMULATOR_BREJNI_H

#include <jni.h>

extern JavaVM *gJVM;

JNIEnv *breGetThreadLocalJNIEnv();

#endif //BREWEMULATOR_BREJNI_H
