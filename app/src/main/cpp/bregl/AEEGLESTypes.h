#ifndef __AEEGLESTYPES_H_
#define __AEEGLESTYPES_H_
typedef unsigned int    AEEGLenum;
typedef unsigned char   AEEGLboolean;
typedef unsigned int    AEEGLbitfield;
typedef signed char     AEEGLbyte;
typedef short           AEEGLshort;
typedef int             AEEGLint;
typedef int             AEEGLsizei;
typedef unsigned char   AEEGLubyte;
typedef unsigned short  AEEGLushort;
typedef unsigned int    AEEGLuint;
typedef float           AEEGLfloat;
typedef float           AEEGLclampf;
typedef void            AEEGLvoid;
typedef int             AEEGLfixed;
typedef int             AEEGLclampx;

typedef int             AEEGLintptr;
typedef int             AEEGLsizeiptr;
typedef int             AEEGLintptrARB;
typedef int             AEEGLsizeiptrARB;

#endif  // __AEEGLESTYPES_H_
