#ifndef AEEHIDDEVICE_JOYSTICK_H 
#define AEEHIDDEVICE_JOYSTICK_H

//==============================================================================
// FILE:         AEEHIDDevice_Joystick.h
//
// SERVICES:     Human Interface Device (HID) Device Unique IDs for joysticks
//
// DESCRIPTION:  UIDs for Joysticks.
// 
//        Copyright (c) 2008 QUALCOMM Incorporated.
//        All Rights Reserved.
//        Qualcomm Confidential and Proprietary
//============================================================================*/

//==============================================================================
//                                 Constants
//==============================================================================

/* Device class Macros */
#define   AEEUID_HID_Joystick_Device    0x0106c3fd

//Joystick Button UIDs
#define   AEEUID_HIDJoystick_DPad_Up                0x0106c3fe
#define   AEEUID_HIDJoystick_DPad_Left              0x0106c3ff
#define   AEEUID_HIDJoystick_DPad_Down              0x0106c400
#define   AEEUID_HIDJoystick_DPad_Right             0x0106c401
#define   AEEUID_HIDJoystick_Start                  0x0106c402
#define   AEEUID_HIDJoystick_Back                   0x0106c403
#define   AEEUID_HIDJoystick_Left_Thumbstick        0x0106c404
#define   AEEUID_HIDJoystick_Right_Thumbstick       0x0106c405
#define   AEEUID_HIDJoystick_Left_Shoulder_Upper    0x0106c406
#define   AEEUID_HIDJoystick_Left_Shoulder_Lower    0x0106c407
#define   AEEUID_HIDJoystick_Right_Shoulder_Upper   0x0106c408
#define   AEEUID_HIDJoystick_Right_Shoulder_Lower   0x0106c409
#define   AEEUID_HIDJoystick_Button_1               0x0106c40a
#define   AEEUID_HIDJoystick_Button_2               0x0106c40b
#define   AEEUID_HIDJoystick_Button_3               0x0106c40c
#define   AEEUID_HIDJoystick_Button_4               0x0106c40d

//Joystick Axes UIDs
#define   AEEUID_HIDJoystick_RightThumb_X           0x0106c4ce
#define   AEEUID_HIDJoystick_RightThumb_Y           0x0106c4cf
#define   AEEUID_HIDJoystick_LeftThumb_X            0x0106c4d0
#define   AEEUID_HIDJoystick_LeftThumb_Y            0x0106c4d1
#define   AEEUID_HIDJoystick_Throttle               0x0106c4d3

/*==============================================================================
DATA STRUCTURES DOCUMENTATION
================================================================================

AEEUID_HID_Joystick_Device

Description: 
   This UID indicates that this device is a joystick.

See Also:
   AEEHIDDeviceInfo
   IHID_GetConnectedDevices()

================================================================================

AEEUID_HIDJoystick_DPad_Up

Description: 
   This UID indicates that the button is the up position on a directional pad.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_DPad_Left

Description: 
   This UID indicates that the button is the left position on a directional pad.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_DPad_Down

Description: 
   This UID indicates that the button is the down position on a directional pad.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Start

Description: 
   This UID indicates that the button is the start button.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Back

Description: 
   This UID indicates that the button is the back button.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Left_Thumbstick

Description: 
   This UID indicates that the button is is the left thumbstick.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Right_Thumbstick

Description: 
   This UID indicates that the button is the right thumbstick.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Left_Shoulder_Upper

Description: 
   This UID indicates that the button is the upper button on the left shoulder
   of the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Left_Shoulder_Lower

Description: 
   This UID indicates that the button is the lower button on the left shoulder
   of the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Right_Shoulder_Upper

Description: 
   This UID indicates that the button is the upper button on the right shoulder
   of the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Right_Shoulder_Lower

Description: 
   This UID indicates that the button is the lower button on the right shoulder
   of the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Button_1

Description: 
   This UID indicates that the button is button 1 on the controller.

See Also:
   AEEHIDPositionInfo

================================================================================

AEEUID_HIDJoystick_Button_2

Description: 
   This UID indicates that the button is button 2 on the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Button_3

Description: 
   This UID indicates that the button is button 3 on the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_Button_4

Description: 
   This UID indicates that the button is button 4 on the controller.

See Also:
   AEEHIDButtonInfo

================================================================================

AEEUID_HIDJoystick_RightThumb_X

Description: 
   This UID indicates that the axis is the x direction on the right thumbstick.

See Also:
   AEEHIDButtonInfo
   IHIDDevice_GetAxesInfo()

================================================================================

AEEUID_HIDJoystick_RightThumb_Y

Description: 
   This UID indicates that the axis is the y direction on the right thumbstick.

See Also:
   AEEHIDPositionInfo
   IHIDDevice_GetAxesInfo()

================================================================================

AEEUID_HIDJoystick_LeftThumb_X

Description: 
   This UID indicates that the axis is the x direction on the left thumbstick.

See Also:
   AEEHIDPositionInfo
   IHIDDevice_GetAxesInfo()

================================================================================

AEEUID_HIDJoystick_LeftThumb_Y

Description: 
   This UID indicates that the axis is the y direction on the left thumbstick.

See Also:
   AEEHIDPositionInfo
   IHIDDevice_GetAxesInfo()

================================================================================

AEEUID_HIDJoystick_Throttle

Description: 
   This UID indicates that the axis is a throttle control.

See Also:
   AEEHIDPositionInfo
   IHIDDevice_GetAxesInfo()

================================================================================
*/

#endif    // AEEHIDDEVICE_JOYSTICK_H

