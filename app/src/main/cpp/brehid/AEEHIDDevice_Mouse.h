#ifndef AEEHIDDEVICE_MOUSE_H 
#define AEEHIDDEVICE_MOUSE_H

//==============================================================================
// FILE:         AEEHIDDevice_Mouse.h
//
// SERVICES:     Human Interface Device (HID) Device Unique IDs for mice
//
// DESCRIPTION:  UIDs for mice.
// 
//        Copyright (c) 2008 QUALCOMM Incorporated.
//        All Rights Reserved.
//        Qualcomm Confidential and Proprietary
//============================================================================*/

//==============================================================================
//                                 Constants
//==============================================================================

/* Device class Macros */
#define   AEEUID_HID_Mouse_Device       0x0106c3fb

/*==============================================================================
DATA STRUCTURES DOCUMENTATION
================================================================================

AEEUID_HID_Mouse_Device

Description: 
   This UID indicates that this device is a mouse.

See Also:
   AEEHIDDeviceInfo
   IHID_GetConnectedDevices()

================================================================================
*/

#endif    // AEEHIDDEVICE_MOUSE_H

