#include "AEEHID.bid"
#include "AEEIHID.h"
#include "AEEHIDDevice_Keyboard.h"
#include "AEEHIDDevice_Mouse.h"
#include "AEEHIDDevice_Joystick.h"
#include "../bre2/breJni.h"
#include <AEEModTable.h>
#include <AEE_OEM.h>
#include <OEMHeap.h>
#include <jni.h>
#include <android/input.h>

#define NUM_LOCAL_REFS 16

static AEEUID androidSourcesToAEEUID(uint32 sources) {
    if (((sources & AINPUT_SOURCE_JOYSTICK) == AINPUT_SOURCE_JOYSTICK) ||
        ((sources & AINPUT_SOURCE_GAMEPAD) == AINPUT_SOURCE_GAMEPAD)) {
        return AEEUID_HID_Joystick_Device;
    } else if ((sources & AINPUT_SOURCE_KEYBOARD) == AINPUT_SOURCE_KEYBOARD) {
        return AEEUID_HID_Keyboard_Device;
    } else if ((sources & AINPUT_SOURCE_MOUSE) == AINPUT_SOURCE_MOUSE) {
        return AEEUID_HID_Mouse_Device;
    } else {
        return AEEUID_HID_Unknown_DeviceType;
    }
}

OBJECT(CHIDDevice) {
    AEEVTBL(IHIDDevice) *pvt;
    uint32 m_nRefs;
    jint m_deviceID;
    jobject m_deviceObject;
};

static uint32 CHIDDevice_AddRef(IHIDDevice *po) {
    CHIDDevice *pMe = (CHIDDevice *) po;

    return (++(pMe->m_nRefs));
}

static uint32 CHIDDevice_Release(IHIDDevice *po) {
    CHIDDevice *pMe = (CHIDDevice *) po;
    if (pMe->m_nRefs) {
        if (--pMe->m_nRefs == 0) {
            sys_free(po);
        }
    }
    return pMe->m_nRefs;
}

static int CHIDDevice_QueryInterface(IHIDDevice *po, AEEIID cls, void **ppo) {
    switch (cls) {
        case AEEIID_QUERYINTERFACE:
        case AEECLSID_HID:
            *ppo = (void *) po;
            CHIDDevice_AddRef(po);
            return SUCCESS;
        default:
            *ppo = NULL;
            return ECLASSNOTSUPPORT;
    }
}

AEEResult CHIDDevice_GetDeviceInfo(IHIDDevice *self_, AEEHIDDeviceInfo *pDevInfo) {
    CHIDDevice *self = (CHIDDevice *) self_;
    JNIEnv *env = breGetThreadLocalJNIEnv();

    jclass inputDeviceClass = (*env)->FindClass(env, "android/view/InputDevice");
    jmethodID getVendorID = (*env)->GetMethodID(env, inputDeviceClass, "getVendorId", "()I");
    jmethodID getProductID = (*env)->GetMethodID(env, inputDeviceClass, "getProductId", "()I");
    jmethodID getSources = (*env)->GetMethodID(env, inputDeviceClass, "getSources", "()I");

    uint32 sources = (uint32) (*env)->CallIntMethod(env, self->m_deviceObject, getSources);
    pDevInfo->nDeviceType = androidSourcesToAEEUID(sources);
    pDevInfo->wVendorID = (*env)->CallIntMethod(env, self->m_deviceObject, getVendorID);
    pDevInfo->wProductID = (*env)->CallIntMethod(env, self->m_deviceObject, getProductID);
    pDevInfo->bBluetoothDevice = FALSE; // Android does not provide this information

    return AEE_SUCCESS;
}

AEEResult CHIDDevice_GetDeviceStatus(IHIDDevice *self, int *pnStatus) {
    *pnStatus = AEE_SUCCESS; // TODO: check for actual status changes
    return AEE_SUCCESS;
}

AEEResult CHIDDevice_RegisterForStatusChange(IHIDDevice *self, ISignal *piSignal) {
    // TODO: check for actual status changes
    return AEE_SUCCESS;
}

AEEResult
CHIDDevice_GetButtonInfo(IHIDDevice *self, int nButtonID, AEEHIDButtonInfo *pnButtonInfo) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetNumberOfButtons(IHIDDevice *self, int *pnButtons) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_RegisterForButtonEvent(IHIDDevice *self, ISignal *piSignal) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetNextButtonEvent(IHIDDevice *self, AEEHIDButtonInfo *pnButtonInfo,
                                        uint32 *pdwTimestamp,
                                        boolean *pbDroppedEvents) {

    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetPositionState(IHIDDevice *self, AEEHIDPositionInfo *pPosInfo) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetMinPositionInfo(IHIDDevice *self, AEEHIDPositionInfo *pPosInfo) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetMaxPositionInfo(IHIDDevice *self, AEEHIDPositionInfo *pPosInfo) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetAxesInfo(IHIDDevice *self, AEEHIDPositionInfo *pPosInfo) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_RegisterForPositionChange(IHIDDevice *self, ISignal *piSignal) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_SetExclusiveLevel(IHIDDevice *self, int nLevel) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_GetExclusiveLevel(IHIDDevice *self, int *pnLevel) {
    return AEE_EUNSUPPORTED;
}

AEEResult CHIDDevice_Rumble(IHIDDevice *self, int nLeftMotorSpeed, int nRightMotorSpeed) {
    return AEE_EUNSUPPORTED;
}

AEEResult
CHIDDevice_GetRumbleStatus(IHIDDevice *self, int *pnLeftMotorSpeed, int *pnRightMotorSpeed) {
    return AEE_EUNSUPPORTED;
}

static const VTBL(IHIDDevice) gsCHIDDeviceFuncs = {
        CHIDDevice_AddRef,
        CHIDDevice_Release,
        CHIDDevice_QueryInterface,
        CHIDDevice_GetDeviceInfo,
        CHIDDevice_GetDeviceStatus,
        CHIDDevice_RegisterForStatusChange,
        CHIDDevice_GetButtonInfo,
        CHIDDevice_GetNumberOfButtons,
        CHIDDevice_RegisterForButtonEvent,
        CHIDDevice_GetNextButtonEvent,
        CHIDDevice_GetPositionState,
        CHIDDevice_GetMinPositionInfo,
        CHIDDevice_GetMaxPositionInfo,
        CHIDDevice_GetAxesInfo,
        CHIDDevice_RegisterForPositionChange,
        CHIDDevice_SetExclusiveLevel,
        CHIDDevice_GetExclusiveLevel,
        CHIDDevice_Rumble,
        CHIDDevice_GetRumbleStatus,
};

OBJECT(CHID) {
    AEEVTBL(IHID) *pvt;
    uint32 m_nRefs;
};

static uint32 CHID_AddRef(IHID *po) {
    CHID *pMe = (CHID *) po;

    return (++(pMe->m_nRefs));
}

static uint32 CHID_Release(IHID *po) {
    CHID *pMe = (CHID *) po;
    if (pMe->m_nRefs) {
        if (--pMe->m_nRefs == 0) {
            sys_free(po);
        }
    }
    return pMe->m_nRefs;
}

static int CHID_QueryInterface(IHID *po, AEEIID cls, void **ppo) {
    switch (cls) {
        case AEEIID_QUERYINTERFACE:
        case AEECLSID_HID:
            *ppo = (void *) po;
            CHID_AddRef(po);
            return SUCCESS;
        default:
            *ppo = NULL;
            return ECLASSNOTSUPPORT;
    }
}

static AEEResult CHID_CreateDevice(IHID *self_, int nDevHandle, IHIDDevice **ppiHidDevice) {
    JNIEnv *env = breGetThreadLocalJNIEnv();

    if (!ppiHidDevice) return AEE_SUCCESS;

    if ((*env)->PushLocalFrame(env, NUM_LOCAL_REFS) < 0) return AEE_ENOMEMORY;

    jclass inputDeviceClass = (*env)->FindClass(env, "android/view/InputDevice");
    jmethodID getDevice = (*env)->GetStaticMethodID(env, inputDeviceClass, "getDevice",
                                                    "(I)Landroid/view/InputDevice;");

    AEEResult result = AEE_SUCCESS;

    jobject device = (*env)->CallStaticObjectMethod(env, inputDeviceClass, getDevice, nDevHandle);
    if (device) {
        CHIDDevice *pNew = (CHIDDevice *) AEE_NewClassEx((IBaseVtbl *) &gsCHIDDeviceFuncs,
                                                         sizeof(CHID), TRUE);
        if (!pNew) {
            result = ENOMEMORY;
        } else {
            pNew->m_nRefs = 1;
            pNew->m_deviceID = nDevHandle;
            pNew->m_deviceObject = (*env)->NewGlobalRef(env, device);

            *ppiHidDevice = (IHIDDevice *) pNew;
            result = AEE_SUCCESS;
        }
    } else {
        result = AEE_ENOSUCH;
    }

    (*env)->PopLocalFrame(env, NULL);

    return result;
}

static AEEResult CHID_GetDeviceInfo(IHID *self, int nDevHandle, AEEHIDDeviceInfo *pDevInfo) {
    IHIDDevice *device = NULL;
    AEEResult result = IHID_CreateDevice(self, nDevHandle, &device);
    if (AEE_SUCCESS == result) {
        result = IHIDDevice_GetDeviceInfo(device, pDevInfo);
        IHIDDevice_Release(device);
    }
    return result;
}

static AEEResult
CHID_GetNextConnectEvent(IHID *self, int *pnDevHandle, int *pnStatus, boolean *pbDroppedEvents) {
    // TODO:
    return EUNSUPPORTED;
}

static AEEResult CHID_RegisterForConnectEvents(IHID *self, ISignal *piSignal) {
    // TODO:
    return EUNSUPPORTED;
}

static AEEResult
CHID_GetConnectedDevices(IHID *self_, int nDeviceType, int *pnDevHandles, int pnDevHandlesLen,
                         int *pnDevHandlesLenReq) {
    JNIEnv *env = breGetThreadLocalJNIEnv();

    AEEResult result = AEE_SUCCESS;

    if ((*env)->PushLocalFrame(env, NUM_LOCAL_REFS) < 0) return AEE_ENOMEMORY;

    jclass inputDeviceClass = (*env)->FindClass(env, "android/view/InputDevice");
    jmethodID getDeviceIDs = (*env)->GetStaticMethodID(
            env, inputDeviceClass, "getDeviceIds", "()[I");
    jmethodID getDevice = (*env)->GetStaticMethodID(
            env, inputDeviceClass, "getDevice", "(I)Landroid/view/InputDevice;");
    jmethodID getSources = (*env)->GetMethodID(
            env, inputDeviceClass, "getSources", "()I");

    jobject deviceEnumeration = (*env)->CallStaticObjectMethod(env, inputDeviceClass, getDeviceIDs);
    jsize totalNumDevices = (*env)->GetArrayLength(env, deviceEnumeration);
    jint *deviceHandles = (*env)->GetIntArrayElements(env, deviceEnumeration, NULL);
    int numValidDevices = 0;
    for (jsize i = 0; i < totalNumDevices; i++) {
        jint handle = deviceHandles[i];
        jobject device = (*env)->CallStaticObjectMethod(env, inputDeviceClass, getDevice, handle);
        uint32 sources = (uint32) (*env)->CallIntMethod(env, device, getSources);
        AEEUID realDeviceType = androidSourcesToAEEUID(sources);

        if (realDeviceType != nDeviceType) continue;

        if (numValidDevices < pnDevHandlesLen && pnDevHandles) {
            pnDevHandles[numValidDevices] = handle;
        }

        numValidDevices++;
    }
    (*env)->ReleaseIntArrayElements(env, deviceEnumeration, deviceHandles, JNI_ABORT);

    if (pnDevHandlesLenReq) {
        *pnDevHandlesLenReq = numValidDevices;
    }

    (*env)->PopLocalFrame(env, NULL);

    return result;
}

static const VTBL(IHID) gsCHIDFuncs = {
        CHID_AddRef,
        CHID_Release,
        CHID_QueryInterface,
        CHID_CreateDevice,
        CHID_GetDeviceInfo,
        CHID_GetNextConnectEvent,
        CHID_RegisterForConnectEvents,
        CHID_GetConnectedDevices,
};

int CHID_New(IShell *ps, AEECLSID ClsId, void **ppObj) {
    CHID *pNew;

    *ppObj = NULL;

    if (ClsId == AEECLSID_HID) {
        pNew = (CHID *) AEE_NewClassEx((IBaseVtbl *) &gsCHIDFuncs,
                                       sizeof(CHID), TRUE);
        if (!pNew) {
            return ENOMEMORY;
        } else {
            pNew->m_nRefs = 1;

            *ppObj = pNew;
            return AEE_SUCCESS;
        }
    }

    return EUNSUPPORTED;
}

// === //

const AEEStaticClass gAEEHIDClasses[] = {
        {AEECLSID_HID, ASCF_UPGRADE, 0, NULL, CHID_New},
        {0, 0,                       0, NULL, NULL}
};
