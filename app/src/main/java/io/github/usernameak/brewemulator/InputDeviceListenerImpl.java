package io.github.usernameak.brewemulator;

import android.annotation.TargetApi;
import android.hardware.input.InputManager;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class InputDeviceListenerImpl implements InputManager.InputDeviceListener {
    @Override
    public native void onInputDeviceAdded(int deviceId);

    @Override
    public native void onInputDeviceRemoved(int deviceId);

    @Override
    public void onInputDeviceChanged(int deviceId) {

    }
}
