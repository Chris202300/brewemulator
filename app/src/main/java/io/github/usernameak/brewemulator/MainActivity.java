package io.github.usernameak.brewemulator;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

public class MainActivity extends Activity {
    static {
        System.loadLibrary("brewemu");
    }

    private ViewGroup keypad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            InputManager im = (InputManager) getSystemService(INPUT_SERVICE);
            im.registerInputDeviceListener(new InputDeviceListenerImpl(), null);
        }

        FrameLayout fr = new FrameLayout(this);

        EmulatorSurfaceView surfaceView = new EmulatorSurfaceView(getApplicationContext());
        surfaceView.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        fr.addView(surfaceView);

        keypad = EmulatorKeypad.buildKeypad(this, new EmulatorKeypad.IKeypadHandler() {
            @Override
            public void onButtonDown(int code) {
                brewEmuOnScreenKeyDown(code);
            }

            @Override
            public void onButtonUp(int code) {
                brewEmuOnScreenKeyUp(code);
            }
        });
        keypad.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, Gravity.BOTTOM));
        fr.addView(keypad);

        fr.setFocusable(false);

        setContentView(fr);

        brewEmuJNIStartup();
    }

    private void hideKeypad() {
        keypad.setVisibility(View.GONE);
    }

    @Override
    public native boolean dispatchKeyEvent(KeyEvent event);

    @Override
    protected void onDestroy() {
        super.onDestroy();

        brewEmuJNIShutdown();
    }

    public void setUseLandscapeOrientation(boolean value) {
        if(value) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    public native boolean brewEmuOnScreenKeyUp(int keyCode);
    public native boolean brewEmuOnScreenKeyDown(int keyCode);

    public native void brewEmuJNIStartup();
    public native void brewEmuJNIShutdown();
}